#%module(package="geomod") geomod
%module geomod

%include "stdint.i"
%include "std_string.i"

// Symbols to generated header
%{
#include <geometry.hh>

int draw_line(std::string fname, int x0, int y0, int x1, int y1);
geometry::point init_point(const int x, const int y);

%}


%include "../geometry.hh"

//int draw_line(std::string fname, int x0, int y0, int x1, int y1);
