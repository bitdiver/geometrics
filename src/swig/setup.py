from distutils.core import setup, Extension

name = "geomod"
version = "1.0"

setup(name=name, 
      version=version,
      packages=["."],
      ext_modules=[
           Extension(name="_geomod", 
              sources=["geomod.i", "../geometry.cc"],
              swig_opts=['-c++', '-py3'],
              include_dirs=["..", "/usr/local/include", "/usr/include/freetype2"],
              libraries = ['PNGwriter',"png","freetype"],
              library_dirs = ['/usr/local/lib'],
              extra_compile_args = ["-fPIC", '-std=c++17']
              )

])
