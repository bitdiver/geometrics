# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.12
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_geomod')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_geomod')
    _geomod = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_geomod', [dirname(__file__)])
        except ImportError:
            import _geomod
            return _geomod
        try:
            _mod = imp.load_module('_geomod', fp, pathname, description)
        finally:
            if fp is not None:
                fp.close()
        return _mod
    _geomod = swig_import_helper()
    del swig_import_helper
else:
    import _geomod
del _swig_python_version_info

try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0

class t_canvas_size(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, t_canvas_size, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, t_canvas_size, name)
    __repr__ = _swig_repr
    __swig_setmethods__["width"] = _geomod.t_canvas_size_width_set
    __swig_getmethods__["width"] = _geomod.t_canvas_size_width_get
    if _newclass:
        width = _swig_property(_geomod.t_canvas_size_width_get, _geomod.t_canvas_size_width_set)
    __swig_setmethods__["height"] = _geomod.t_canvas_size_height_set
    __swig_getmethods__["height"] = _geomod.t_canvas_size_height_get
    if _newclass:
        height = _swig_property(_geomod.t_canvas_size_height_get, _geomod.t_canvas_size_height_set)

    def __init__(self):
        this = _geomod.new_t_canvas_size()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _geomod.delete_t_canvas_size
    __del__ = lambda self: None
t_canvas_size_swigregister = _geomod.t_canvas_size_swigregister
t_canvas_size_swigregister(t_canvas_size)

class point(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, point, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, point, name)
    __repr__ = _swig_repr
    __swig_setmethods__["x"] = _geomod.point_x_set
    __swig_getmethods__["x"] = _geomod.point_x_get
    if _newclass:
        x = _swig_property(_geomod.point_x_get, _geomod.point_x_set)
    __swig_setmethods__["y"] = _geomod.point_y_set
    __swig_getmethods__["y"] = _geomod.point_y_get
    if _newclass:
        y = _swig_property(_geomod.point_y_get, _geomod.point_y_set)

    def __init__(self):
        this = _geomod.new_point()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _geomod.delete_point
    __del__ = lambda self: None
point_swigregister = _geomod.point_swigregister
point_swigregister(point)
cvar = _geomod.cvar
canvas_small = cvar.canvas_small
canvas_middle = cvar.canvas_middle

class line(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, line, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, line, name)
    __repr__ = _swig_repr
    __swig_setmethods__["p0"] = _geomod.line_p0_set
    __swig_getmethods__["p0"] = _geomod.line_p0_get
    if _newclass:
        p0 = _swig_property(_geomod.line_p0_get, _geomod.line_p0_set)
    __swig_setmethods__["p1"] = _geomod.line_p1_set
    __swig_getmethods__["p1"] = _geomod.line_p1_get
    if _newclass:
        p1 = _swig_property(_geomod.line_p1_get, _geomod.line_p1_set)

    def __init__(self):
        this = _geomod.new_line()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _geomod.delete_line
    __del__ = lambda self: None
line_swigregister = _geomod.line_swigregister
line_swigregister(line)

class color(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, color, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, color, name)
    __repr__ = _swig_repr
    __swig_setmethods__["r"] = _geomod.color_r_set
    __swig_getmethods__["r"] = _geomod.color_r_get
    if _newclass:
        r = _swig_property(_geomod.color_r_get, _geomod.color_r_set)
    __swig_setmethods__["g"] = _geomod.color_g_set
    __swig_getmethods__["g"] = _geomod.color_g_get
    if _newclass:
        g = _swig_property(_geomod.color_g_get, _geomod.color_g_set)
    __swig_setmethods__["b"] = _geomod.color_b_set
    __swig_getmethods__["b"] = _geomod.color_b_get
    if _newclass:
        b = _swig_property(_geomod.color_b_get, _geomod.color_b_set)

    def __init__(self):
        this = _geomod.new_color()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _geomod.delete_color
    __del__ = lambda self: None
color_swigregister = _geomod.color_swigregister
color_swigregister(color)


def png_line_from_points(image: 'pngwriter &', p0: 'point', p1: 'point', c: 'color') -> "void":
    return _geomod.png_line_from_points(image, p0, p1, c)
png_line_from_points = _geomod.png_line_from_points
class engine(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, engine, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, engine, name)
    __repr__ = _swig_repr

    def __init__(self, s: 't_canvas_size', fname: 'std::string const &', bg: 'double const'=1.0):
        this = _geomod.new_engine(s, fname, bg)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this

    def draw_grid(self) -> "void":
        return _geomod.engine_draw_grid(self)

    def draw_lines(self) -> "void":
        return _geomod.engine_draw_lines(self)

    def line_from_points(self, p0: 'point', p1: 'point') -> "void":
        return _geomod.engine_line_from_points(self, p0, p1)
    __swig_destroy__ = _geomod.delete_engine
    __del__ = lambda self: None
engine_swigregister = _geomod.engine_swigregister
engine_swigregister(engine)


def draw_line(fname: 'std::string', x0: 'int', y0: 'int', x1: 'int', y1: 'int') -> "int":
    return _geomod.draw_line(fname, x0, y0, x1, y1)
draw_line = _geomod.draw_line
# This file is compatible with both classic and new-style classes.


