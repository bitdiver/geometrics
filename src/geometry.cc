
#include <iostream>
#include <pngwriter.h>

#include "geometry.hh"

using namespace std;


// functions
void geometry::png_line_from_points(pngwriter & image, const geometry::point & p0, const geometry::point & p1, const geometry::color & c ) {

    geometry::line l{p0, p1};

    image.line(
        l.p0.x, l.p0.y, 
        l.p1.x, l.p1.y, 
        c.r, c.g, c.b
    );
}



void geometry::engine::draw_grid() {
  int h = image->getheight();
  int w = image->getwidth();

  int delta_y = h/10;
  int delta_x = w/10;

  geometry::color c0{0xaa00, 0xaa12, 0xaa22};
  for (int i=0; i < h; i += delta_y) {
    png_line_from_points(*image, geometry::point{0,i}, geometry::point{ w, i}, c0);
  }

  for (int i=0; i < w; i += delta_x) {
    png_line_from_points(*image, geometry::point{i,0}, geometry::point{ i, h}, c0);
  }
}

void geometry::engine::line_from_points(const point & p0, const point & p1 ) {
  geometry::color c0{0xaa00, 0xa002, 0x0002};
  png_line_from_points(*image, p0, p1, c0);
}


void geometry::engine::draw_lines() {
  geometry::point p0{5, 10};
  geometry::point p1{300, 90};

  geometry::point p2{150, 120};

  geometry::color c0{255, 20, 200};
  png_line_from_points(*image, p0, p1, c0);

  geometry::color c1{255, 20, 100};
  png_line_from_points(*image, p1, p2, c1);

  geometry::color c2{255, 240, 20};
  png_line_from_points(*image, p2, p0, c2);
}

void geometry::engine::draw_point(int x, int y) {
  image->plot(x, y, 0.0, 0.0, 1.0);
}


int draw_line(std::string fname, int x0, int y0, int x1, int y1) {
  geometry::engine engine(fname, geometry::canvas_small);
  
  engine.draw_grid();
  geometry::point p0{x0, y0};
  geometry::point p1{x1, y1};

  engine.line_from_points(p0, p1);

  
  return 2;
};

geometry::point init_point(const int x, const int y) {
  return geometry::point { x, y };
}
