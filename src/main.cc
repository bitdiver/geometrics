
#include <iostream>

#include "geometry.hh"

using namespace std;

// functions


// main
int main(int argn, char ** argv) {
  cout << "Generate geometries... " << endl;
  if (argn < 2) {
    cout << "Please add output filename." << endl;
    return 1;
  }

  string fname(argv[1]);
  geometry::engine engine(fname, geometry::canvas_small);
  
  engine.draw_grid();
  // engine.draw_lines();
  //engine.

  cout << "Saved image: " << fname << endl;
  
  return 0;
}
