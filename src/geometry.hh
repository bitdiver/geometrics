
#pragma once

#include <iostream>
#include <memory>


#include <pngwriter.h>

using namespace std;


namespace geometry {

  // data structures
  struct t_canvas_size {
    int width;
    int height;
  }; 

  const struct t_canvas_size canvas_small = { 320, 200 };
  const struct t_canvas_size canvas_middle = { 640, 480 };

  struct point {
    int x;
    int y;
  };

  struct line {
    point p0;
    point p1;
  };

  struct color {
    int r;
    int g;
    int b;
  };


  // helper functions
  //
  // write to a png image and 
  void png_line_from_points(pngwriter & image, const point & p0, const point & p1, const color & c ); 

  // classes
  class engine {

    private:

      // image properties
      string fname;
      t_canvas_size canvas_size;
      double background_color;

      unique_ptr<pngwriter> image;

      bool closed;

    public:

      engine(const string & fname, const t_canvas_size s = canvas_small , const double bg = 1.0) : fname(fname), canvas_size(s), background_color(bg) {
        image = std::make_unique<pngwriter>(s.width, s.height, bg, fname.c_str());
        closed = false;
      }

      void draw_grid();

      void draw_point(int x, int y);

      void draw_lines();

      void line_from_points(const point & p0, const point & p1 ); 

      void close() { image->close(); closed = true; }

      ~engine() {
        if (!closed) {
          image->close();
        }
      }
  };

  point init_point(const int x, const int y);
}

int draw_line(string fname, int x0, int y0, int x1, int y1);



