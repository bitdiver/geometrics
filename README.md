
# Geometrics

The goal of this library is to render shapes from a code. To render the graphics the [pngwriter](https://github.com/pngwriter/pngwriter) library.

A Python interface based on swig is provided to work with graphics and geometry in an interactive mode.

An example is shown below:
<img src="./images/geometry_example.png" />

The project uses pngwriter library to render png files.
Both libraries have to be compile with -fPIC to make the Swig module work.


## Batch mode

You can generate geometric images with:

  bin/main <fname.png>


## Python mode

Local build:

    python3 setup.py build --build-platlib .

Using venv:

    python3 -m venv local

## Lines

First example generates lines and segments



